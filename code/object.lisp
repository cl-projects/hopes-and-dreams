#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams)

(defgeneric lookup-message (object selector))
(defgeneric lookup-implicit-message (implicit-self selector))
(defclass object () ())

(defclass standard-object (object)
  ((%object-map
    :initarg :object-map :reader object-map
    :accessor %object-map
    :initform (make-instance 'standard-map))
   (%object-vector
    :reader object-vector
    :initarg :object-vector
    :initform (make-array 3 :fill-pointer 0 :adjustable t))))

(defgeneric find-slot (map selector))
(defgeneric parent-slots (map))
(defclass standard-map ()
  ((%slot-descriptions
    :initarg :slot-descriptions
    :initarg :effective-slots
    :reader slot-descriptions
    :initform '() :type list)
   (%transitions
    :initarg :transitions :accessor transitions
    :type list :initform '())))

(defmethod find-slot ((map standard-map) selector)
  (find selector (slot-descriptions map) :key #'message-selector))

(defmethod parent-slots (map)
  (remove-if-not #'slot-parent-p (slot-descriptions map)))

(defmethod lexical-parent-slots (map)
  (remove-if-not #'slot-lexical-parent-p (slot-descriptions map)))

;;; TODO
;;; Needs to be better but it's just a literal translation from the
;;; self handbook.
(defun %walk-parent-objects (object selector visited-objects)
  (if (member object visited-objects)
      (values nil (cons object visited-objects))
      (let ((local-slot (find-slot (object-map object) selector)))
        (push object visited-objects)
        (cond
          (local-slot
           (values (list (cons local-slot object))
                   visited-objects))

          (t (let ((slots '()))
               (dolist (parent (parent-slots (object-map object)))
                 (multiple-value-bind (slot-set next-visited-objects)
                     (%walk-parent-objects (slot-location parent)
                                           selector
                                           visited-objects)
                   (setf visited-objects
                         next-visited-objects)
                   (unless (null slot-set)
                     (setf slots
                           (append slot-set slots)))))
               (values slots visited-objects)))))))

(defmethod lookup-message ((object standard-object) selector)
  (macrolet ((slot (cons) `(car ,cons))
             (object (cons) `(cdr ,cons)))
    (let ((slots (%walk-parent-objects object selector nil)))
      ;; TODO ambiguous mesage send should be raised
      (assert (>= 1 (length slots)))
      (let ((result (first slots)))
        (values (slot result) (object result))))))

(defmethod lookup-implicit-message ((implicit-self standard-object)
                                    selector)
  ;; bruh i cba
  nil)

(defgeneric find-transition (transition-node descriptor)
  (:documentation "Used by transition to find a map if the description
has already been added to this map before.")
  (:method ((transition-node standard-map) descriptor)
    (cdr (assoc descriptor (transitions transition-node)))))

(defgeneric transition (map descriptor)
  (:documentation "Provide a new map containing the provided
slot description")
  (:method ((map standard-map) descriptor)
    (let ((existing-map (find-transition map descriptor)))
      (or existing-map
          (let ((next-map
                  (make-instance 'standard-map
                                 :slot-descriptions
                                 (cons descriptor
                                       (slot-descriptions map)))))
            (setf (transitions map)
                  (acons descriptor next-map (transitions map)))
            next-map)))))

;;; objects are always mutable, their maps however ARE NOT.
(defgeneric add-slot (object slotd)
  (:method ((object standard-object) slotd)
    (let ((next-map (transition (object-map object) slotd)))
      (setf (%object-map object) next-map)
      (when (slot-assignable-p slotd)
        ;; TODO CHANGE THIS UNBOUND BS.
        (vector-push-extend 'unbound (object-vector object)))
      object)))
