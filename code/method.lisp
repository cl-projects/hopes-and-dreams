#|
    Copyright (C) 2020-2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams)

(defclass cl-method ()
  ((%cl-function
    :initarg :cl-function
    :reader cl-function
    :initform (alexandria:required-argument))
   (%arguments
    :initarg :arguments
    :reader cl-function-arguments
    :initform (alexandria:required-argument)
    :documentation "FIXME Don't know what the machine format
for arguments is yet. But this should be be whatever is used
for normal methods once that is decided.")))

(defclass cl-method-slot-description (o:standard-slot-description)
  ((%cl-method :initarg :cl-method :reader cl-method)))

(defgeneric method-code (method))
(defgeneric method-literals (method))
(defgeneric method-arguments (method))
(defgeneric find-literal (index method))
(defclass method (standard-object)
  ((%code :initarg :method-code :reader method-code)
   (%literals :initarg :method-literals :reader method-literals)
   (%arguments :initarg :method-arguments :reader method-arguments
               :initform (make-array 3 :adjustable t :fill-pointer 0)
               :documentation "A vector of argument slots in the order
they should be removed from the execution stack.")
   (%uir :initarg :method-uir :reader method-uir :initform '()
         :documentation "This is only really here because
we don't want to  deserialize it from source or bytecode just yet
on the fly.")))

(defmethod find-literal (index (method method))
  (aref (method-literals method) index))

(defclass standard-argument-slot (o:standard-slot-description)
  ())

(defun make-argument-slot (selector)
  (make-instance
   'standard-argument-slot
   :selector selector
   :assignablep t))
