#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:hopes-and-dreams.sourcerer
  (:use #:cl)
  (:export
   #:read-from-string*
   #:read-from-file
   #:read-from-file*)
  (:local-nicknames
   (#:us #:utena-symbols)
   (#:h #:hopes-and-dreams)
   (#:noir #:hopes-and-dreams.noir)
   (#:cst #:concrete-syntax-tree)
   (#:concrete #:hopes-and-dreams.sourcerer.concrete)))
