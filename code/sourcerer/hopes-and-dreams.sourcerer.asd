(cl:in-package #:asdf-user)

(defsystem #:hopes-and-dreams.sourcerer
  :author "Gnuxie <gnuxie@applied-langua.ge>"
  :serial t
  :depends-on
  ("cl-change-case" "hopes-and-dreams" "alexandria"
                    "concrete-syntax-tree"
                    "concrete-syntax-tree-source-info"
                    "concrete-syntax-tree-destructuring"
                    "hopes-and-dreams.noir")
  :components
  ((:module "concrete"
    :components
    ((:file "concrete")))
   (:file "package")
   (:file "syntax")
   (:file "reader")
   (:file "method-compile")
   (:file "expansion")
   (:file "method-macros")
   (:file "class")))
