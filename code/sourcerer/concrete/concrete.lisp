#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:hopes-and-dreams.sourcerer.concrete
  (:use #:cl)
  (:local-nicknames
   (#:cst #:concrete-syntax-tree))
  (:export
   #:dumb-source-info
   #:position
   #:document

   #:make-dumb-source-info))

(in-package #:hopes-and-dreams.sourcerer.concrete)

;;; We would like to be able to use CST's standard-source-info
;;; https://github.com/s-expressionists/Concrete-Syntax-Tree/blob/master/Source-info/source-info.lisp
;;; but we can't find Eclector using it which sucks.
;;; They only use a stream's file-position.
(defclass dumb-source-info (concrete-syntax-tree-source-info::source-info)
  ((%position :initarg :position :reader stream-position)
   (%document :initarg :document :reader document)))

(defun make-dumb-source-info (document position)
  (make-instance
   'dumb-source-info
   :position position :document document))
