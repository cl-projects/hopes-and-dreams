#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams.object)

(defclass slot-description-protocol () ())


;;; usually one of instance, class or lexical-parent.
;;; it's an optimization to put lexical-parent into
;;; effective-slot definitions...
;;; I don't think we're going to in our case.
(defgeneric slot-allocation (slot-description))
(defgeneric slot-assignable-p (slot-description))
(defgeneric slot-selector (slot-description))
(defgeneric slot-access (slot-description))

(defclass standard-slot-description (slot-description-protocol)
  ((%slot-selector
    :initarg :selector
    :reader slot-selector
    :initform (alexandria:required-argument))
   (%slot-assignable-p
    :initarg :assignablep
    :reader slot-assignable-p
    :initform nil)
   (%slot-allocation
    :initarg :allocation
    :reader slot-allocation
    :initform :instance)
   (%slot-access
    :initarg :access
    :reader slot-access
    ;; Note: You're free to do whatever you want in your language
    ;; and have the default be public. But not here mate.
    :initform :private)))
