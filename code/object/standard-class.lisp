#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams.object)

;;; mixins themselves don't need to be classes.
;;; they'e always applied to something else.
;;; they just need to ansewer to the direct-slot protoocl.



(defclass utena-system-class ()
  ())

;;; The only method we have on a class currently is its
;;; constructor, so we don't need to store anything else
;;; in the class. Methods applicable to instances of the
;;; class are stored in the effective slots sequence.
(defclass utena-standard-class (utena-system-class)
  ((%effective-slots
    :initarg :effective-slots
    :reader class-effective-slots
    :initform (alexandria:required-argument))
   (%class-superclass
    :initarg :superclass
    :reader class-superclass
    :initform nil)
   (%class-mixins
    :initarg :mixins
    :reader class-mixins
    :initform '())
   (%class-constructor
    :initarg :constructor
    :reader class-constructor
    :initform (alexandria:required-argument))))

(defclass utena-standard-object ()
  ((%class
    :initarg :class
    :reader class
    :initform (alexandria:required-argument))
   (%object-vector
    :initarg :object-vector
    :reader object-vector
    :initform (make-array 3 :fill-pointer 0 :adjustable t))))

;;; class definition mixin time.
;;; As class-definition-mixins are intended to be transported
;;; transparently between VMs it is not possible for them to be
;;; responsible for tracking their own dependants.
;;; We therefore want to inroduce a third components later on
;;; that will provide this tracking and inform classes
;;; when their mixins have a successor.
;;; Not that class-definition-mixins could be trusted to track
;;; their own dependants if they could be provided as
;;; standard objects anyways.

;;; dependency tracker shouldn't really be part of the MOP
;;; it should just be transparent about whether it exists or not.
(defclass dependency-tracker () ())

(defgeneric add-applicant (tracker mixin class)
  (:method (tracker mixin class) nil))
(defgeneric add-direct-subclass (tracker class subclass)
  (:method (tracker mixin class) nil))

;;; remember this is totally internal, effective slots
;;; shouldn't exist to perspective of anyone but effective class internals.
(defun clone-slot-initargs (slotd)
  (list :selector (slot-selector slotd)
        :assignablep (slot-assignable-p slotd)
        :allocation (slot-allocation slotd)
        :access (slot-access slotd)))

(defun effective-slots<-final-slotds (slotds)
  (let ((slot-location 0))
    (mapcar
     (lambda (slotd)
       (ecase (slot-allocation slotd)
         (:instance
          (prog1
              (apply
               #'make-instance
               (list* :location slot-location
                      (clone-slot-initargs slotd)))
            (incf slot-location)))
         (:class
          (apply #'make-instance
                 'standard-effective-class-slot
                 (clone-slot-initargs slotd)))))
     slotds)))

(defgeneric make-effective-slot (standard-slot-description &rest args)
  (:method ((slotd slot-description-protocol) &rest args)
    (let ((misc-args
            ))
      )))

(defclass standard-effective-slot ())

(defclass standard-effective-instance-slot
    (standard-effective-slot standard-slot-description)
  ((%slot-location
    :initarg :location
    :reader slot-location
    :initform (alexandria:required-argument))))

(defclass standard-effective-class-slot
    (standard-effective-slot standard-slot-description)
  ((%slot-data
    :initarg :data
    :accessor slot-data)))

(defun apply-slots (super-slots application-slots)
  (let ((next-slots super-slots))
    (loop for slot in application-slots
          for existing-slot = (find slot super-slots :key #'message-selector)
          if (null existing-slot)
            do (push slot next-slots)
          else do (remove existing-slot super-slots)
               and do (push slot next-slots))
    next-slots))

(defun make-effective-class (class-definition
                                 superclass
                                 mixins)
  (let* ((super-mix-slots
           (list* (or superclass (effective-slots superclass))
                  (mapcar direct-slots mixins)))
         (effective-slots
           (loop with next-slots = '()
                 for mixin in super-mix-slots
                 do (setf next-slots (apply-slots next-slots
                                                  mixin))
                 finally (return-from nil next-slots)))
         (new-class
           (make-instance
            'standard-class
            :effective-slots (effective-slots<-final-slotds effective-slots)
            :class-superclass superclass
            :class-mixins mixins)))
    (add-direct-subclass nil superclass new-class)
    (dolist (mixin mixins)
      ;; n.b if ADD-APPLICANT calls into user methods, we
      ;; probably should make sure the send has UNWIND-PROTECT
      ;; so that the user can't unwind past MAKE-E-CLASS and
      ;; ruin everything. We could add the UNWIND-PROTECT to
      ;; every send done by WITH-UTENA-SENDS so that we're safe
      ;; from that kind of thing by default; and then unwinding
      ;; could be /enabled/ explicitly, when it is desired, and that's
      ;; only safe when the send is the final thing to do.
      ;; (And, to be fair, we'd want basically the same thing regardless
      ;; of implementation strategy; we need to self-preserve in
      ;; metacircular Utena too. But then do we really trust the user
      ;; to maintain our dependencies?)
      (add-applicant nil mixin new-class))
    new-class))

;;; we shouldn't trust user class-definition-mixins to track
;;; their own dependencies.
;;; likewise i think it's bad for class-definitoin-mixins
;;; to track their dependants in the first place
;;; because it stops them being transported safely.
;;; something else has to be responsible for it.
;;; bruh, the fact that we send messages here really means that this
;;; module is not distinct from the VM, maybe.
(defgeneric produce-class
    (class-definition-mixin enclosing-object)
  (:method (c-d-mixin enclosing-object)
    (with-utena-sends
        ;; the class-definition-mixin has to have block templates
        ;; and these need to be initialized when their enclosing object
        ;; is (and produced into a class like this)
        ((super-mix-block (c-d-mixin 'us::super-mix-block))
         (super-mix (super-mix-block 'us::value enclosing-object)))
      (:protect-last-p nil)
      (let ((superclass (getf super-mix-block :super))
            (mixins (progn (remf super-mix-block :super)
                           super-mix-block)))
        ;; bruh remf and getf are such shit.
        ;; wish to change super-mix syntax asap ffs.
        (setf super-mix-block :used)
        (make-effective-class c-d-mixin superclass mixins)))))
