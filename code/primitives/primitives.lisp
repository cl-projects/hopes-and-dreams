#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams)

(defparameter *platform*
  (make-instance
   'standard-object
   :object-map (make-instance 'standard-map)))

(defmacro define-platform-entry (selector entry)
  `(let ((data-slot
           (make-data-slot
            ',selector :assignablep nil :lexical-access-control :public
            :initial-value ,entry)))
     (add-slot *platform* data-slot)))

(defvar *forwarded-functions* (make-hash-table :test 'equal))

(defun get-forwarded-function (selector system-class)
  (gethash (cons selector system-class) *forwarded-functions*))

(defun (setf get-forwarded-function) (new-value selector system-class)
  (setf (gethash (cons selector system-class) *forwarded-functions*)))

(defmethod lookup-message ((object t) selector)
  (get-forwarded-function selector (class-of object)))

(defmacro define-forwarded-method (utena-selector cl-system-class function-name)
  `(let* ((arguments (trivial-arguments:arglist (function ',function-name)))
          (method-arguments (mapcar #'make-argument-slot arguments)))
     (setf (get-forwarded-function ',utena-selector
                                   (find-class ',cl-system-class))
           (make-instance
            'primitive-forwarded-function-slot
            :forwarded-function-name ',function-name
            :message-selector ',utena-selector
            :forwarded-function (function ',function-name)))))

;;; ok so the issue is we kind of need introspective database
;;; of system-classes and selector dispatch and how
;;; to evlauate those messages.
;;; It would be simpler i think to encode these in an object
;;; map with a primitive function slot just bc it's consistent
;;; whether the machine uses this doesn't matter it'll be slow af
;;; but it's just simple af so idc rn.

;;; OK this is dumb bc if we were planning to inject it via
;;; defmethod it'd still be dumb af.
;;; but at the same time, it is the best way of storing stuff so idk.
;;; Cos it could be used to build whatever dispatch mechanism
;;; we have. probably a jump table in the default method.

(defclass primitive-forwarded-function-slot (standard-slot-description)
  ((%forwarded-function-name
    :initarg :forwarded-function-name :reader forwarded-function-name
    :documentation "This can be nil, it's just this might be useful later.")
   (%forwarded-function :initarg :forwarded-function :reader forwarded-function)))

(defmethod evaluate-message ((slotd primitive-forwarded-function-slot)
                             object
                             machine)
  (error "Unimplemented."))
