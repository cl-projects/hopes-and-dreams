#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams.noir)

(defclass uir ()
  ())

(defclass abstract-send (uir)
  ((%selector-part :initarg :selector-part :reader selector-part)
   (%argument-parts :initarg :argument-parts :reader argument-parts
                    :initform '())
   (%explicit-argument-count
    :initarg :explicit-argument-count :reader explicit-argument-count
    :documentation
    "This also includes the selector and target parts, it's what
was given in the operand.")))

;;; can probably cheese implicit self send into this too
(defclass send (abstract-send)
  ((%receiver-part :initarg :receiver-part :reader receiver-part)))

(defun make-send (selector-part receiver-part &rest argument-parts)
  (make-instance
   'send
   :receiver-part receiver-part
   :selector-part selector-part
   :argument-parts argument-parts))

(defclass implicit-self-send (abstract-send) ())

(defun make-implicit-self-send (selector-part &rest argument-parts)
  (make-instance
   'implicit-self-send
   :selector-part selector-part
   :argument-parts argument-parts))

(defclass drop (uir) ())

(defun make-drop () (make-instance 'drop))

(defclass self (uir) ())

(defun make-self () (make-instance 'self))

(defclass abstract-constant (uir)
  ((%value :initarg :value :reader value)))

(defclass literal (abstract-constant) ())

;;; should this really be called make-literal
;;; and not load-literal?
(defun make-literal (literal)
  (make-instance 'literal :value literal))

;;; technically constant is just bs
;;; it's a shared table and it's not for us to figure
;;; out what the contents are, it's an optimization.
(defclass constant (abstract-constant) ())

;;; there is also constant send
;;; apply prefix
;;; and tail call prefix.
