#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams)
;;;; OK, I have no idea how any of this bullshit works right now
;;;; but I do know how it should work, so i'm going to write it down.
;;;; A class-definition-mixin has a super-mix clause
;;;; if the class-definition-mixin is evaluated in a context
;;;; where it is at the top (it is the top level), then
;;;; it must be evaluated as though it has been finalized in the context
;;;; of an enclosing object (with no environment).
;;;; So how does finalization work?
;;;; When an enclosing (finalized-class)'s factory method is called
;;;; then all of the class-definition-mixin's that are immediatley nested
;;;; in that class will get finalized as part of the (finalized-class's)
;;;; instantiation process. This happens by evaluating their super-mix
;;;; clause within the context of the factory method (well
;;;; technically like cl `:around` which means it also has access
;;;; to the same lexical scope as the factory method (it runs before
;;;; it though ala :around) similar to how slots are initialized,
;;;; which is how these classes are being finalized because
;;;; nested classes belong in slots and are finalized from their
;;;; class-definition-mixins as part of the classes's instantiation
;;;; process. NOw I need to ask Hayley wtf we said about the order
;;;; of slot initialization and whether they could depend on each other
;;;; fuck.



;;; redefinition protocol needs to decide when and how to upgrade.
;;; A mixin might appear safe to upgrade all the time,
;;; but that isnt' true if the subclasses made assumptions
;;; about the interface and the interface has been changed.
;;; In this case, a more involved update and migration stratagy is required.
;;; To avoid

(defclass class-protocol () ())

(defclass class-dependency-management-mixin ()
  ((%direct-subclasses :initarg :direct-subclasses
                       :accessor direct-subclasses
                       :initform '())
   (%direct-applicants
    :initarg :direct-applicants :accessor direct-applicants
    :initform '() :documentation "The classes that are the result of this
definition being applied to another class as a mixin (including as a superclass).")))

(defgeneric add-applicant (mixin class)
  (:method ((mixin class-dependency-management-mixin) class)
    (push class (direct-applicants mixin))))

(defgeneric add-direct-subclass (class subclass)
  (:method ((class class-dependency-management-mixin) subclass)
    (push subclass (direct-subclasses class))))

;;; The only thing here that isn't a product of finalization is direct-slots
(defclass class-definition
    (class-protocol class-dependency-management-mixin)
  ((%direct-slots :initarg :direct-slots :reader direct-slots
                  :initform '())
   (%superclass-clause :initarg :superclass-clause :reader superclass-clause
                       :initform nil)
   (%mixin-precedence-list
    :initarg :mixin-precedence-list :reader mixin-precedence-list
    :initform '())
   (%successor :accessor successor :initform nil)
   ;; should be some kind of canonical class allocated slot.
   (%constructor :accessor :constructor :initform nil)))

(defclass standard-class
    (class-protocol class-dependency-management-mixin standard-map)
  ((%class-superclass :initarg :class-direct-superclass :reader
                      class-direct-superclass)
   (%class-mixins :initarg :class-mixins :accessor class-mixins
                  :initform '())
   (%successor :reader successor :initform nil))
  (:documentation "We don't necessarily need this to be
a distinct primitive, it would be fine as a standard object
with class-mixins,successor,superclass as private slots
with a hidden selector."))

(defmethod apply-slots (super-slots application-slots)
  (let ((next-slots super-slots))
    (loop for slot in application-slots
       for existing-slot = (find slot super-slots :key #'message-selector)
       if (null existing-slot)
       do (push slot next-slots)
       else do (remove existing-slot super-slots)
       and do (push slot next-slots))
    next-slots))

(defmethod make-effective-class ((class class-definition)
                                 superclass
                                 mixins)
  (let* ((effective-slots
          (loop with next-slots = '()
             for mixin in (list* superclass mixins)
             do (setf next-slots (apply-slots next-slots
                                              (direct-slots mixin)))
             finally (return-from nil next-slots)))
         (new-class
          (make-instance
           'standard-class
           :effective-slots effective-slots
           :class-superclass superclass
           :class-mixins mixins)))
    (add-direct-subclass superclass new-class)
    (dolist (mixin mixins)
      (add-applicant mixin new-class))
    new-class))

;;;; F A C T O R Y      M E T H O D

;;; it'll be better if this wrapped the method
;;; rather than did this, just because it makes
;;; calling the same method on the standard-class
;;; easier later.
(defclass factory-method-slot (standard-slot-description)
  ((%factory-method-description :initarg :method-slot-description)))

(defun wrap-method-in-factory-slot (method-slot)
  (make-instance
   'factory-method-slot
   :method-slot-description method-slot
   :message-selector (message-selector method-slot)
   :slot-assignable-p nil
   :lexical-access-control (slot-lexical-access-control method-slot)))

;;; i feel like the part that evaluates each block is best
;;; as a utena maethod.
;;; otherwise we are winding the host stack and it's the only way
;;; yeah we need to do that or make callbacks in the VM
;;; smh.
;;; nah, just turn it into ne method that initialized the slots once.
;;; nah we can't even do that cos it's complicated af.
;;; JUST WIND THE HOST STACK.
;;; you could also just send a hidden message to assign the slots
;;; once.
;;; na just wind the stack, if someone finds that it'll b confusing af.

;;; so we concluded winding the host stack was a massive pita
;;; and we wanted to transform thse blocks into something else instead
;;; wrote a massive CST fuckoff and noir (no IR lwl).
(defmethod evaluate-message
    ((slotd factory-method-slot) class-definition receiver machine)
  (declare (ignore receiver))
  ;; evaluate the superclass clasue
  ()
  ;; evaluate the mixin clauses
  ;; make the effecive claass
  (let ((standard-class (make-effective-class class-definition)))
    ;; call evaluate message on the standard class with the original
    ;; method slot.
    ;; put the new standard class on the stack.
    ;; make sure the blocks, constructor do not put
    ;; anything onto the data-stack.
    ;; maybe some kind of stack guard that uses eq from before and after
    ;; with an assertion
    ;; with-stack-guard
    ))
