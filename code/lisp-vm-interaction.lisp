#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:hopes-and-dreams.utena.interaction
  (:use #:cl)
  (:local-nicknames
   (#:us #:utena-symbols)
   (#:h #:hopes-and-dreams))
  (:export
   #:*machine*
   #:send))

(in-package #:hopes-and-dreams.utena.interaction)

(defvar *machine* (make-instance 'h::standard-machine))

(defmacro send ((selector place) &rest arguments)
  `(progn
     (h::foreign-send-message ,place ',selector *machine* ,@arguments)
     (h::pop-data-stack *machine*)))
