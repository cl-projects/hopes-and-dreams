#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams)

(defgeneric make-activated-method (method escape-record self)
  (:documentation "Make an activation record from the method."))
(defclass activation-record (method)
  ((%instruction-pointer
     :initarg :instruction-pointer :accessor instruction-pointer
     :initform 0 :type unsigned-byte)
   (%escape-record
    :initarg :escape-record :reader escape-record
    :initform nil)
   (%self :initarg :activated-self :reader activated-self)))

(defmethod make-activated-method ((method method) escape-record self)
  (make-instance
   'activation-record
   :object-map
   ;; if these sorts of transitions are memoized
   ;; it will be useless bc the selector is always inaccessible
   ;; and unique.
   (transition (object-map method)
               (make-lexical-parent-slot self))
   ;; this is not sufficient, the copy needs to be deep.
   ;; or all ARs will share the same slot arguments.
   ;; What? they're not map allocated though.
   :object-vector (alexandria:copy-array (object-vector method))
   :method-code (method-code method)
   :method-literals (method-literals method)
   :method-arguments (method-arguments method)
   :escape-record escape-record
   :activated-self self))

(defclass standard-method-slot (allocated-slot-description) ())

(defmethod evaluate-message ((slotd standard-method-slot)
                             object
                             receiver
                             machine)
  ;; get the method
  (let* ((method (slot-location-value slotd object))
         (activation-record
           (make-activated-method
            method (activated-method machine) receiver)))
    ;; pop the arguments from the stack
    ;; technically we need to validate that
    ;; the argumenst provided are the same.
    (loop for argument across (method-arguments activation-record)
          do (setf (slot-location-value argument activation-record )
                   (pop-data-stack machine)))
    ;; activate the activation record
    ;; we return it to the machine for now so they can do it.
    activation-record))

(defun make-method-slot
    (selector method &key (lexical-access-control :private))
  (make-instance
   'standard-method-slot
   :message-selector selector
   :slot-location method
   :slot-assignable-p nil
   :lexical-access-control lexical-access-control))

;;; Dispatch loopy stuff
(declaim (inline high-nibble low-nibble operand-byte))
(defun high-nibble (bytecode)
  (ldb (byte 4 12) bytecode))

(defun low-nibble (bytecode)
  (ldb (byte 4 8) bytecode))

(defun operand-byte (bytecode)
  (ldb (byte 8 0) bytecode))

(defun call-method-block (machine method-block)
  ;; we have a problem
  (let ((activation
          (or (null (activated-method machine))
              (activated-method machine))))
    ()))
