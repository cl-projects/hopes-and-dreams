(cl:in-package #:asdf-user)

(defsystem #:hopes-and-dreams.test.sourcerer
  :serial t
  :depends-on ("hopes-and-dreams.sourcerer" "parachute")
  :components
  ((:file "package")
   (:file "define-class")
   (:file "syntax")
   (:file "fib")))
