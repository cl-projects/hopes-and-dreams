#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams.test.sourcerer)

(defvar *classes* (make-hash-table))

(defmacro define-class (name program)
  `(let* ((cst
            (s::method-macrowalk
             (s::read-from-string* ',program)))
          (top-class
            (hopes-and-dreams.sourcerer::parse-class cst)))
     (setf (gethash ',name *classes*)
           top-class)))

(defun find-sourcerer-class (name)
  (gethash name *classes*))
