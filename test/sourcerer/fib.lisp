#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(cl:in-package #:hopes-and-dreams.test.sourcerer)

(define-class fib-test "
(class fib-test ()
  ((cons))
  (constructor new (cons*)
    (set! cons cons*))

  (method fib (n)
    (let ((numbers '())
          (a 1)
          (b 1))
      (while-true:
        (<: (length: numbers) n)
        [()
         (let ((temp b))
           (set! numbers (cons a numbers))
           (set! b (+: a b))
           (set! a temp))])
       numbers)))
")

(define-test fib
  (let* ((fib-test-class (find-sourcerer-class 'fib-test))
         (fib-test (i:send (us::new fib-test-class) #'cons))
         (fib (i:send (us::fib fib-test) 7)))
    (is #'= fib 13)))
