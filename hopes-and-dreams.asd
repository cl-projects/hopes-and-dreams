(cl:in-package #:asdf-user)

(defsystem #:hopes-and-dreams
  :author "Gnuxie <gnuxie@applied-langua.ge>"
  :serial t
  :depends-on ("trivial-arguments" "alexandria" "hopes-and-dreams.object")
  :components
  ((:module "code" :components
            ((:file "package")
             (:file "slot-description")
             (:file "object")
             (:file "method")
             (:file "block")
             (:file "class")
             (:file "machine")
             (:file "activation")
             (:file "instruction")
             (:file "instructions")
             (:file "instruction-dispatch")
             (:file "lisp-vm-interaction")))))
