## Hopes and Dreams

This is a bootstrap interpreter for [Utena VM](https://cal-coop.gitlab.io/utena/utena-specification/main.pdf).

We don't really have any aims for it other than to bootstrap a metacircular implementation of Utena.
